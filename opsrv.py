#!/usr/bin/env python
##
##############################################################################
#
# @file       opnode.py
# @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2011.
# @brief      Base ROS-UAVObject connector node
#   
# @see        The GNU Public License (GPL) Version 3
#
#############################################################################/
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#


import rospy
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Imu, Temperature, MagneticField
from std_srvs.srv import Empty, EmptyResponse
from hector_uav_msgs.msg import Altimeter
import time, sys
import math
import serial
import traceback
from openpilot.uavtalk.uavobject import *
from openpilot.uavtalk.uavtalk import *
from openpilot.uavtalk.objectManager import *
from openpilot.uavtalk.connectionManager import *
    
from threading import Thread

class OPConnector:

    def __init__(self):
    	rospy.init_node('openpilot_node')
	port = "/dev/ttyACM0" #rospy.get_param("~port")
	prefix = "test" #rospy.get_param("~prefix")
        self._port = port

	self._serPort = serial.Serial(port, 57600, timeout=.5)
        if not self._serPort.isOpen():
            raise IOError("Failed to open serial port")
        self.uavTalk = UavTalk(self._serPort, None)
        
        print "Starting ObjectManager"
        self.objMan = ObjManager(self.uavTalk)
        self.objMan.importDefinitions()
        
        print "Starting UavTalk"
        self.uavTalk.start()

	self._prefix = prefix
        self._pubImu = rospy.Publisher(prefix + "/imu", Imu, queue_size=5)
        self._pubMag = rospy.Publisher(prefix + "/magnetic_field", MagneticField, queue_size=5)
        self._pubAltimeter = rospy.Publisher(prefix + "/Altimeter", Altimeter, queue_size=5)
        self._pubTemp = rospy.Publisher(prefix + "/temperature", Temperature, queue_size=5)
	self._pubPose = rospy.Publisher(prefix + "/Pose", Pose, queue_size=5)
#        self._pubBattery = rospy.Publisher(tf_prefix + "/battery", Float32, queue_size=10)
	self.setObservers()
	rate = rospy.Rate(10) # 10hz
	while not rospy.is_shutdown():
		rate.sleep()
#	self.stop()

    def setObservers(self):
        print "Request fast periodic updates for AttitudeState"
#        self.objMan.AttitudeState.metadata.telemetryUpdateMode.value = UAVMetaDataObject.UpdateMode.PERIODIC
#        self.objMan.AttitudeState.metadata.telemetryUpdatePeriod.value = 50
#        self.objMan.AttitudeState.metadata.updated()
	self.objMan.regObjectObserver(self.objMan.MagState, self, "onMagUpdate")
	self.objMan.regObjectObserver(self.objMan.AccelState, self, "onImuUpdate")
        self.objMan.regObjectObserver(self.objMan.AttitudeState, self, "onPoseUpdate")
	self.objMan.regObjectObserver(self.objMan.BaroSensor, self, "onBaroSensorUpdate")

    def onMagUpdate(self, args):
	msg = MagneticField()
        msg.header.stamp = rospy.Time.now()
	msg.magnetic_field.x = self.objMan.MagState.x.value
        msg.magnetic_field.y = self.objMan.MagState.y.value
        msg.magnetic_field.z = self.objMan.MagState.z.value
	self._pubMag.publish(msg)

    def onBaroSensorUpdate(self, args):
	msg = Altimeter()
        msg.header.stamp = rospy.Time.now()
	msg.altitude = self.objMan.BaroSensor.Altitude.value
	msg.pressure = self.objMan.BaroSensor.Pressure.value
	self._pubAltimeter.publish(msg)

	msg = Temperature()
        msg.header.stamp = rospy.Time.now()
        msg.temperature = self.objMan.BaroSensor.Temperature.value
        self._pubTemp.publish(msg)

    def onPoseUpdate(self, args):
	msg = Pose()
	msg.position.x = self.objMan.PositionState.North.value
	msg.position.y = self.objMan.PositionState.East.value
	msg.position.z = 0 - self.objMan.PositionState.Down.value
	
	msg.orientation. w = self.objMan.AttitudeState.q1.value
	msg.orientation. x = self.objMan.AttitudeState.q2.value
	msg.orientation. y = self.objMan.AttitudeState.q3.value
	msg.orientation. z = self.objMan.AttitudeState.q4.value
	self._pubPose.publish(msg)	

    def onImuUpdate(self, args):
	msg = Imu()
        # ToDo: it would be better to convert from timestamp to rospy time
        msg.header.stamp = rospy.Time.now()
        #msg.header.frame_id = self.tf_prefix + "/base_link"

        # measured in deg/s; need to convert to rad/s
        msg.linear_acceleration.x = self.objMan.AccelState.x.value
        msg.linear_acceleration.y = self.objMan.AccelState.y.value
        msg.linear_acceleration.z = self.objMan.AccelState.z.value

        # measured in mG; need to convert to m/s^2
        msg.angular_velocity.x = math.radians(self.objMan.GyroState.x.value)
        msg.angular_velocity.y = math.radians(self.objMan.GyroState.y.value)
        msg.angular_velocity.z = math.radians(self.objMan.GyroState.z.value)

	msg.orientation. w = self.objMan.AttitudeState.q1.value
	msg.orientation. x = self.objMan.AttitudeState.q2.value
	msg.orientation. y = self.objMan.AttitudeState.q3.value
	msg.orientation. z = self.objMan.AttitudeState.q4.value

        self._pubImu.publish(msg)	

    def stop(self):
        if self.uavTalk:
            print "Stopping UavTalk"
            self.uavTalk.stop()

if __name__ == '__main__':
    try:
        OPConnector()	
    except rospy.ROSInterruptException:
        pass
